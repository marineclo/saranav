//
//  BuildConfiguration.swift
//  SARANav
//
//  Created by Marine on 13.09.23.
//  Copyright © 2023 GraniteApps. All rights reserved.
//

import Foundation

enum Environment: String { // 1
    case debugDevelopment = "Debug"
    case releaseProd = "Release"
    case testFlight = "TestFlight"
}

class BuildConfiguration { // 2
    static let shared = BuildConfiguration()
    
    var environment: Environment
    
    init() {
        let currentConfiguration = Bundle.main.object(forInfoDictionaryKey: "Configuration") as! String
        
        environment = Environment(rawValue: currentConfiguration)!
    }
}
