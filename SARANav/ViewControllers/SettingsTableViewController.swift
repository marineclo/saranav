//
//  SettingsTableViewController.swift
//  SARANav
//
//  Created by Marine on 28.05.21.
//  Copyright © 2021 GraniteApps. All rights reserved.
//

import UIKit
import SARAFramework

class SettingsTableViewController: UITableViewController {

    let bundle = Utils.bundle(anyClass: SettingsTableViewController.classForCoder())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("menu_settings", bundle: bundle, comment: "")
        
//        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.register(UINib(nibName: "ImageCell", bundle: Bundle(for: SettingsTableViewController.self)), forCellReuseIdentifier: "menuCellIdentifier")

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 4
        case 1:
            return 3
        default:
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let imageCell = tableView.dequeueReusableCell(withIdentifier: "menuCellIdentifier") as? ImageCell else {
            return UITableViewCell()
        }
        
        imageCell.accessibilityTraits = UIAccessibilityTraits.button
        imageCell.accessoryType = .disclosureIndicator
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                imageCell.setTitle(title: NSLocalizedString("menu_general", bundle: bundle, comment: ""))
                imageCell.setImage(image: UIImage(named: "helm", in: bundle, compatibleWith: nil))
            case 1:
                imageCell.setTitle(title: NSLocalizedString("menu_nmea", bundle: bundle, comment: ""))
                imageCell.setImage(image: UIImage(named: "central", in: bundle, compatibleWith: nil))
            case 2:
                imageCell.setTitle(title: NSLocalizedString("menu_penons", bundle: bundle, comment: ""))
                imageCell.setImage(image: UIImage(named: "telltale", in: bundle, compatibleWith: nil))
            case 3:
                imageCell.setTitle(title: NSLocalizedString("menu_map_data", bundle: bundle, comment: ""))
                imageCell.setImage(image: UIImage(named: "map", in: bundle, compatibleWith: nil))
            default: break
            }
        case 1:
            switch indexPath.row {
            case 0:
                imageCell.setTitle(title: NSLocalizedString("menu_traces", bundle: bundle, comment: ""))
                imageCell.setImage(image: UIImage(named: "binoculars", in: bundle, compatibleWith: nil))
            case 1:
                imageCell.setTitle(title: NSLocalizedString("menu_about", bundle: bundle, comment: ""))
                imageCell.setImage(image: UIImage(named: "anchor", in: bundle, compatibleWith: nil))
            case 2:
                imageCell.setTitle(title: NSLocalizedString("menu_developer", bundle: bundle, comment: ""))
                imageCell.setImage(image: UIImage(named: "anchor", in: bundle, compatibleWith: nil))
            default: break
            }
        default:
            break
        }
        return imageCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                performSegue(withIdentifier: "generalSegue", sender: nil)
            case 1:
                performSegue(withIdentifier: "nmeaSegue", sender: nil)
            case 2:
                performSegue(withIdentifier: "tellTalesSettingsSegue", sender: nil)
            case 3:
                performSegue(withIdentifier: "mapDataSegue", sender: nil)
            default: break
            }
        case 1:
            switch indexPath.row {
            case 0:
                performSegue(withIdentifier: "tracesListSegue", sender: nil)
                break
            case 1:
                performSegue(withIdentifier: "aboutSegue", sender: nil)
            case 2:
                performSegue(withIdentifier: "developerSegue", sender: nil)
            default: break
            }
        default:
            break
        }
    }
}
