//
//  CreateRouteViewController.swift
//  SARANav
//
//  Created by Marine on 06.10.23.
//  Copyright © 2023 GraniteApps. All rights reserved.
//

import UIKit
import SARAFramework
import RxSwift

class CreateRouteViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, SelectButtonCellDelegate, AccessibleSwitch {
    
    //MARK: - Outlets
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var validateButton: UIBarButtonItem!
    @IBOutlet weak var titleItem: UINavigationItem!

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet private weak var nameTxtField: UITextField!
    
    @IBOutlet weak var addWaypointButton: UIButton!
    @IBOutlet weak var addMarkButton: UIButton!
    @IBOutlet weak var inversedPointsView2: AccessibilitySwitchView!
    
    @IBOutlet weak var reoganisePointsButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var visualizeButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var activateButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    //MARK: - Variables
    let bundle = Utils.bundle(anyClass: CreateRouteViewController.classForCoder())
    var viewModel: CreateCruiseCourseViewModel?
    let disposeBag = DisposeBag()

    //MARK: - ViewController override
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if self.viewModel == nil {
            self.viewModel = CreateCruiseCourseViewModel(route: nil)
        }
        
        if self.viewModel?.getCourseName() != nil {
            self.titleItem.title = Utils.localizedString(forKey: "edit_the_route", app: "SARANav")
        } else {
            self.titleItem.title = Utils.localizedString(forKey: "create_route", app: "SARANav")
        }
        self.cancelButton.title = NSLocalizedString("cancel", bundle: bundle, comment: "")
        self.validateButton.title = NSLocalizedString("ok", bundle: bundle, comment: "")
        
        self.nameLabel.text = NSLocalizedString("name", bundle: bundle, comment: "")
        self.nameLabel.isAccessibilityElement = false
        
        self.nameTxtField.text = self.viewModel?.getCourseName()
        self.nameTxtField.accessibilityLabel = NSLocalizedString("name", bundle: bundle, comment: "")
        self.nameTxtField.delegate = self

        self.nameTxtField
            .rx.text
            .map { $0?.isNotEmpty ?? false }
            .bind(to: self.validateButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        self.nameTxtField
            .rx.text
            .map { $0?.isNotEmpty ?? false }
            .bind(onNext: { isName in
                self.activateButton.isEnabled = isName && ((self.viewModel?.numberOfPoints() ?? 0) >= 1)
                self.uploadButton.isEnabled = isName && User.shared.email.value != nil && ((self.viewModel?.numberOfPoints() ?? 0) >= 1)
            }).disposed(by: disposeBag)
        
        // Not possible to change the name of the route if there is the one enabled
        self.nameTxtField.isEnabled = NavigationManager.shared.route.value == self.viewModel?.route ? false : true
        self.validateButton.isEnabled = !(nameTxtField.text?.isEmpty ?? true)
        
        self.tableView.register(UINib(nibName: "HeaderTableView", bundle: Bundle(for: CreateRouteViewController.classForCoder())), forHeaderFooterViewReuseIdentifier: "HeaderTableView")
        self.tableView?.register(UINib(nibName: "SelectButtonCell", bundle: Bundle(for: CreateRouteViewController.classForCoder())), forCellReuseIdentifier: "chooseCourseCellIdentifier")
        self.tableView.customize()
        
        setupButton()
        
        self.inversedPointsView2.delegate = self
        
        // Close keyboard when tap on the view
        let tapGesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.elementsToLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(handleModalDismissed), name: NSNotification.Name(rawValue:"modalCreatePointIsDimissed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleModalDismissed), name: NSNotification.Name(rawValue:"modalCreateMarkIsDimissed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleModalDismissed), name: NSNotification.Name("modalChoosePointIsDimissed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onClose), name: NSNotification.Name("modalEnablePoint"), object: nil)
    }

    public override func viewDidDisappear(_ animated: Bool) {
        // Remove observers
//        NotificationCenter.default.removeObserver(self)
        // if the enabled course is this one and there is no points in the course. Set the enbled course to nil
        let cruiseCourse = NavigationManager.shared.route.value as? CruiseCourse
        if self.viewModel?.route == cruiseCourse && self.viewModel?.numberOfPoints() == 0 {
            NavigationManager.shared.setCurrentRoute(route: nil)
        }
    }

    @objc func handleModalDismissed() {
        self.tableView.reloadData()
        self.elementsToLoad()
    }
    
    @objc func onClose(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func elementsToLoad() {
        self.visualizeButton.isEnabled = ((viewModel?.numberOfPoints() ?? 0) >= 2)
        self.activateButton.isEnabled = ((viewModel?.numberOfPoints() ?? 0) >= 1) && self.nameTxtField.text?.isNotEmpty ?? false
        self.uploadButton.isEnabled = User.shared.email.value != nil && ((viewModel?.numberOfPoints() ?? 0) >= 1) && self.nameTxtField.text?.isNotEmpty ?? false
        if self.viewModel?.route == NavigationManager.shared.route.value {
            self.viewModel?.currentPointIndex = NavigationManager.shared.currentPointIndex.value
        }
    }
    
    func setupButton() {
        self.addWaypointButton.customizePrimary()
        self.addWaypointButton.setTitle(NSLocalizedString("add_course_point", bundle: bundle, comment: ""), for: .normal)
        self.addWaypointButton.accessibilityLabel = NSLocalizedString("add_course_point", bundle: bundle, comment: "")
        self.addMarkButton.customizePrimary()
        self.addMarkButton.setTitle(NSLocalizedString("add_mark", bundle: bundle, comment: ""), for: .normal)
        self.addMarkButton.accessibilityLabel = NSLocalizedString("add_mark", bundle: bundle, comment: "")
        
        self.inversedPointsView2.customizeView(cornerRadius: 10)
        self.inversedPointsView2.titleLabel.text = NSLocalizedString("inverse", bundle: bundle, comment: "")
        self.inversedPointsView2.settingSwitch.isOn = self.viewModel?.reversedRoute ?? false
        self.inversedPointsView2.settingSwitch.tintColor = Utils.secondaryColor
        self.inversedPointsView2.settingSwitch.onTintColor = Utils.primaryColor
        self.inversedPointsView2.settingSwitch.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        
        self.reoganisePointsButton.customizeSecondary()
        self.reoganisePointsButton.setTitle(NSLocalizedString("reorganize", bundle: bundle, comment: ""), for: .normal)
        self.reoganisePointsButton.accessibilityLabel = NSLocalizedString("reorganize", bundle: bundle, comment: "")
        
        self.visualizeButton.customizeSecondary()
        self.visualizeButton.setTitle(NSLocalizedString("visualize", bundle: bundle, comment: ""), for: .normal)
        self.visualizeButton.accessibilityLabel = NSLocalizedString("visualize", bundle: bundle, comment: "")
        // A course can only be displayed if there are at least 2 points
        self.visualizeButton.isEnabled = ((viewModel?.numberOfPoints() ?? 0) >= 2)

        self.uploadButton.customizeSecondary()
        self.uploadButton.setTitle(NSLocalizedString("upload", bundle: bundle, comment: ""), for: .normal)
        self.uploadButton.accessibilityLabel = NSLocalizedString("upload", bundle: bundle, comment: "")
        // A course can only be shared if there is at least 1 point, a name for the course and the user is connected
        self.uploadButton.isEnabled = User.shared.email.value != nil && self.nameTxtField.text?.isNotEmpty ?? false && ((viewModel?.numberOfPoints() ?? 0) >= 1)
        
        self.activateButton.customizeSecondary()
        // A course can only be activated if there is at least 1 point
        self.activateButton.isEnabled = ((viewModel?.numberOfPoints() ?? 0) >= 1) && self.nameTxtField.text?.isNotEmpty ?? false
        if let course = self.viewModel?.route, course == NavigationManager.shared.route.value {
            self.activateButton.setTitle(NSLocalizedString("deactivate", bundle: bundle, comment: ""), for: .normal)
            self.activateButton.accessibilityLabel = NSLocalizedString("deactivate", bundle: bundle, comment: "")
        } else {
            self.activateButton.setTitle(NSLocalizedString("activate", bundle: bundle, comment: ""), for: .normal)
            self.activateButton.accessibilityLabel = NSLocalizedString("activate", bundle: bundle, comment: "")
        }
        
        self.deleteButton.customizeSecondary()
        self.deleteButton.setTitle(NSLocalizedString("delete", bundle: bundle, comment: ""), for: .normal)
        self.deleteButton.accessibilityLabel = NSLocalizedString("delete", bundle: bundle, comment: "")
        if self.viewModel?.route.id == "" {
            self.deleteButton.isEnabled = false
        } else {
            self.deleteButton.isEnabled = true
            self.deleteButton.setTitleColor(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1), for: .normal)
        }
    }
    
    public func setCourse(_ course: Course) {
        if let cruiseCourse = course as? CruiseCourse {
            self.viewModel = CreateCruiseCourseViewModel(route: cruiseCourse)
        }
    }
    
    private func saveCourseAndGoNav() {
        if let name = self.nameTxtField.text, !name.isEmpty {
            if viewModel?.route.name == "" {
                viewModel?.saveRoute(name: name)
            } else {
                viewModel?.updateRoute(name: name)
            }
        }
        self.activateButton.setTitle(NSLocalizedString("deactivate", bundle: bundle, comment: ""), for: .normal)
        self.activateButton.accessibilityLabel = NSLocalizedString("deactivate", bundle: bundle, comment: "")
        NavigationManager.shared.updateNavView = true
        NavigationManager.shared.setCurrentRoute(route: viewModel?.route)
    }
    
    private func stopEditing() {
        self.tableView.isEditing = !self.tableView.isEditing
        let title = self.tableView.isEditing ? NSLocalizedString("reorganize_points_validate", bundle: bundle, comment: "") : NSLocalizedString("reorganize", bundle: bundle, comment: "")
        self.reoganisePointsButton.setTitle(title, for: .normal)
        self.reoganisePointsButton.accessibilityLabel = self.tableView.isEditing ? NSLocalizedString("reorganize_points_validate", bundle: bundle, comment: "") : NSLocalizedString("reorganize", bundle: bundle, comment: "")
    }
    
    //MARK: - TextField
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        // Handle focus of voiceover properly
        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: textField)
        return false
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let specialCharacters = "!@#$%&*()+{}[]|\"<>,.~/:;?-=\\¥£•¢"
        let char = CharacterSet(charactersIn: specialCharacters)
        if let _ = string.rangeOfCharacter(from: char) {
            let alert = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("firebase_name_special_char", bundle: bundle, comment: ""), preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("ok", bundle: bundle, comment: ""), style: .cancel, handler: { _ in
                textField.text = (textField.text ?? "") + string.dropLast()
            })
            alert.addAction(okAction)
            self.present(alert, animated: false)
            return false
        }
        return true
    }
    
    // MARK: - Segue
    public override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "createMarkSegue" {
            if let vm = viewModel, vm.reversedRoute {
//                let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("warning_add_mark_reversed", bundle: bundle, comment: ""), preferredStyle: .alert)
                let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: Utils.localizedString(forKey: "warning_add_mark_reversed", app: "SARANav"), preferredStyle: .alert)
                let noAction = UIAlertAction(title: NSLocalizedString("ok", bundle: bundle, comment: ""), style: .cancel, handler: nil)
                alertController.addAction(noAction)
                present(alertController, animated: true, completion: nil)
                return false
            } else {
                return true
            }
        }
        return true
    }
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "createMarkSegue" {
            guard let destination = segue.destination as? AddPointSegmentedViewController else {
                return
            }
            let storyboard = UIStoryboard(name: "Route", bundle: bundle)
            //Instantiate View Controller
            let createMarkVC = storyboard.instantiateViewController(withIdentifier: "CreateMarkViewController") as! CreateMarkViewController
            createMarkVC.viewModel = self.viewModel
            destination.viewControllersList.append(createMarkVC)
            let existingPoint = storyboard.instantiateViewController(withIdentifier: "ChoosePointViewController") as! ChoosePointViewController
            existingPoint.viewModel = self.viewModel
            existingPoint.isMarks = true
            destination.viewControllersList.append(existingPoint)
        } else if segue.identifier == "addPointSegmentedSegue" {
            guard let destination = segue.destination as? AddPointSegmentedViewController else {
                return
            }
            let storyboard = UIStoryboard(name: "Route", bundle: bundle)
            //Instantiate View Controller
            let createPointVC = storyboard.instantiateViewController(withIdentifier: "CreatePointViewController") as! CreatePointViewController
            createPointVC.viewModel = self.viewModel
            destination.viewControllersList.append(createPointVC)
            let existingPoint = storyboard.instantiateViewController(withIdentifier: "ChoosePointViewController") as! ChoosePointViewController
            existingPoint.viewModel = self.viewModel
            existingPoint.isMarks = false
            destination.viewControllersList.append(existingPoint)
        } else if segue.identifier == "visualizeCourseTable" {
            guard self.viewModel?.numberOfPoints() ?? 0 >= 2 else {
                return
            }
            guard let destination = segue.destination as? CourseVisualizationViewController else {
                return
            }
            destination.viewModel = viewModel
            destination.titleNavigation =  nameTxtField.text
        }
    }
    
    // MARK: - IBActions
    @IBAction func reorganizePoints(_ sender: Any) {
        stopEditing()
    }
    
//    dismiss
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateCruiseIsDimissed"), object: nil)
        })
    }
        
    // Validate and save modifications done to the route
    @IBAction func validate(_ sender: Any) {
        if let name = self.nameTxtField.text, !name.isEmpty {
            if self.viewModel?.route.name == "" {
                self.viewModel?.saveRoute(name: name)
            } else {
                self.viewModel?.updateRoute(name: name)
            }
            if self.viewModel?.route == NavigationManager.shared.route.value {
                // set the route
                NavigationManager.shared.setCurrentRoute(route: self.viewModel?.route)
                if let currentPointIndex = self.viewModel?.currentPointIndex {
                    NavigationManager.shared.setPoint(with: currentPointIndex)
                }
            }
            self.dismiss(animated: true, completion: {
                  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateCruiseIsDimissed"), object: nil)
            })
        }
    }
        
    // If available, upload the current course on Firebase
    @IBAction func uploadRouteAction(_ sender: Any) {
        guard let viewModel = self.viewModel, let name = self.nameTxtField.text else {
            return
        }
        if FirebaseManager.shared.reachability.connection == .unavailable {
            let alert = Utils.showAlert(with: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("no_internet_connection_no_sharing", bundle: bundle, comment: ""))
            present(alert, animated: false)
        } else {
            viewModel.route.name == "" ? viewModel.saveRoute(name: name) : viewModel.updateRoute(name: name)
            FirebaseManager.shared.saveCourse(course: viewModel.route, update: false, successHandler: {
                self.present(Utils.showAlert(with: "", message: Utils.localizedString(forKey: "firebase_success_upload", app: "SARANav")), animated: true, completion: nil)
            }, errorHandler: { error in
                if let customError = error as? CustomError, customError == CustomError.firebaseNameAlreadyExists {
                    let alert = Utils.showAlert(with: NSLocalizedString("error", bundle: self.bundle, comment: ""), message: Utils.localizedString(forKey: "firebase_name_exists", app: "SARANav"))
                    self.present(alert, animated: false)
                } else if let customError = error as? CustomError, customError == CustomError.firebaseNameAlreadyExistsOwner {
                    let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: self.bundle, comment: ""), message: Utils.localizedString(forKey: "firebase_name_exists_owner", app: "SARANav"), preferredStyle: .alert)
                    let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: self.bundle, comment: ""), style: .default) { [weak self] _ in
                        alertController.dismiss(animated: true, completion: nil)
                        FirebaseManager.shared.saveCourse(course: viewModel.route, update: true, successHandler: {
                            self?.present(Utils.showAlert(with: "", message: Utils.localizedString(forKey: "firebase_success_upload", app: "SARANav")), animated: true, completion: nil)
                        }, errorHandler: { error in
                            self?.present(Utils.showAlert(with: NSLocalizedString("error", bundle: self!.bundle, comment: ""), message: error.localizedDescription), animated: true, completion: nil)
                        })
                    }
                    let noAction = UIAlertAction(title: NSLocalizedString("no", bundle: self.bundle, comment: ""), style: .cancel, handler: nil)
                    alertController.addAction(noAction)
                    alertController.addAction(yesAction)
                    self.present(alertController, animated: true, completion: nil)
                } else {
                    self.present(Utils.showAlert(with: NSLocalizedString("error", bundle: self.bundle, comment: ""), message: error.localizedDescription), animated: true, completion: nil)
                }
            })
        }
    }
    
    @IBAction func activateRouteAction(_ sender: Any) {
        guard let viewModel = self.viewModel else {
            return
        }
        if viewModel.route == NavigationManager.shared.route.value { // Click on the disable button
            NavigationManager.shared.setCurrentRoute(route: nil)
            self.activateButton.setTitle(NSLocalizedString("activate", bundle: bundle, comment: ""), for: .normal)
            self.activateButton.accessibilityLabel = NSLocalizedString("activate", bundle: bundle, comment: "")
            self.nameTxtField.isEnabled = true
            return
        }
        
        if viewModel.numberOfPoints() == 0 {
            let alert = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: Utils.localizedString(forKey: "warning_no_point_route", app: "SARANav"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("ok", bundle: bundle, comment: ""), style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            
        } else if !viewModel.allPointsHasCoordinates() {
            let alert = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: Utils.localizedString(forKey: "warning_route_point_no_coordinates", app: "SARANav"), preferredStyle: .alert)
            let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { _ in
                self.saveCourseAndGoNav()
                self.dismiss(animated: true, completion: nil)
            }
            let noAction = UIAlertAction(title: NSLocalizedString("no", bundle: bundle, comment: ""), style: .cancel, handler: nil)
            alert.addAction(noAction)
            alert.addAction(yesAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            self.saveCourseAndGoNav()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func deleteRouteAction(_ sender: Any) {
        
        if let route = self.viewModel?.route, let points = self.viewModel?.getTempPoints(), points.isEmpty {
            let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: Utils.localizedString(forKey: "warning_delete_route", app: "SARANav"), preferredStyle: .alert)
            let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { _ in
                route.removeCourse()
                self.dismiss(animated: true, completion: {
                      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateCruiseIsDimissed"), object: nil)
                })
            }
            let noAction = UIAlertAction(title: NSLocalizedString("cancel", bundle: bundle, comment: ""), style: .cancel, handler: nil)
            alertController.addAction(noAction)
            alertController.addAction(yesAction)
            present(alertController, animated: true, completion: nil)
        } else {
            let actionSheet = UIAlertController(title: Utils.localizedString(forKey: "delete_route", app: "SARANav"), message: nil, preferredStyle: .actionSheet)
            actionSheet.addAction(UIAlertAction(title: Utils.localizedString(forKey: "delete_route_keep_points", app: "SARANav"), style: .destructive, handler: { (_) in
                if let route = self.viewModel?.route {
                    route.removeCourse()
                    self.dismiss(animated: true, completion: {
                          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateCruiseIsDimissed"), object: nil)
                    })
                }
            }))
            actionSheet.addAction(UIAlertAction(title: Utils.localizedString(forKey: "delete_route_and_points", app: "SARANav"), style: .destructive, handler: { (_) in
                if let route = self.viewModel?.route {
                    // Check if points are used in another routes. If not delete point, otherwise keep it and add a message to the user.
                    let pointsUsedRoutes = route.removeCourseWithPoints(points: self.viewModel?.getTempPoints() ?? [])
                    if pointsUsedRoutes.count > 0 {
                        let message = pointsUsedRoutes.count == 1 ? String(format: Utils.localizedString(forKey: "point_used_routes_not_deleted", app: "SARANav"), pointsUsedRoutes.first!.name) : String(format: Utils.localizedString(forKey: "points_used_routes_not_deleted", app: "SARANav"), pointsUsedRoutes.map({$0.name}).joined(separator: ", "))
                        let alert = UIAlertController(title: NSLocalizedString("warning", bundle: self.bundle, comment: ""), message: message, preferredStyle: .alert)
                        let okAction = UIAlertAction(title: NSLocalizedString("ok", bundle: self.bundle, comment: ""), style: .cancel, handler: {_ in
                            self.dismiss(animated: true, completion: {
                                  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateCruiseIsDimissed"), object: nil)
                            })
                        })
                        alert.addAction(okAction)
                        self.present(alert, animated: true)
                    } else {
                        self.dismiss(animated: true, completion: {
                              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateCruiseIsDimissed"), object: nil)
                        })
                    }
                }
            }))
            actionSheet.addAction(UIAlertAction(title: NSLocalizedString("cancel", bundle: bundle, comment: ""), style: .cancel, handler: nil))

            self.present(actionSheet, animated: true, completion: nil)
        }
    }
    
    // MARK: - TableView
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
        
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableView" ) as! HeaderTableView
        returnedView.customizeHeader(titleName: NSLocalizedString("points_list", bundle: bundle, comment: ""))
        return returnedView
    }

    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.numberOfPoints() ?? 0
    }
    

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "chooseCourseCellIdentifier") as? SelectButtonCell else {
            return UITableViewCell()
        }
        let point = self.viewModel?.getPointAtIndexPath(indexPath)
        cell.delegate = self
        cell.titleLabel?.font = UIFont.preferredFont(forTextStyle: .body)
        cell.titleLabel?.text = point?.name
        cell.selectButton?.isAccessibilityElement = false
        cell.selectButton?.tintColor = Utils.tintIcon
        if let _ = point as? Mark, let image = UIImage(named: "buoys", in: bundle, compatibleWith: nil) {
            cell.selectButton?.setImage(image, for: .normal)
            cell.accessibilityLabel = "\(point?.name ?? ""). \(NSLocalizedString("mark", bundle: bundle, comment: ""))"
        } else if let _ = point as? CoursePoint, let image = UIImage(named: "course-point", in: bundle, compatibleWith: nil) {
            cell.selectButton?.setImage(image, for: .normal)
            cell.accessibilityLabel = "\(point?.name ?? ""). \(NSLocalizedString("waypoint", bundle: bundle, comment: ""))"
        }
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Check informations of the selected point
        let storyboard = UIStoryboard(name: "Route", bundle: Bundle(for: CreateRouteViewController.classForCoder()))
        //Instantiate View Controller
        if let point = self.viewModel?.getPointAtIndexPath(indexPath) as? CoursePoint {
            guard let controller = storyboard.instantiateViewController(withIdentifier: "CreatePointViewController") as? CreatePointViewController else {
                    return
            }
            controller.setPoint(point)
            controller.viewModel = self.viewModel
            controller.indexInRoute = indexPath.row
            self.present(controller, animated: true, completion: nil)
        } else if let mark = self.viewModel?.getPointAtIndexPath(indexPath) as? Mark {
            guard let controller = storyboard.instantiateViewController(withIdentifier: "CreateMarkViewController") as? CreateMarkViewController else {
                    return
            }
            controller.setMark(mark)
            controller.viewModel = self.viewModel
            controller.indexInRoute = indexPath.row
            self.present(controller, animated: true, completion: nil)
        }
    }

    public func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }

    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

        //Support the deletion of the points in the route
        if editingStyle == .delete {
            // Desactivate route when the point is used in the current route
            if let point = self.viewModel?.getPointAtIndexPath(indexPath), let route = NavigationManager.shared.route.value, self.viewModel?.route == route, route.points.contains(where: {$0.primaryKey == point.id})  {
                var message = ""
                if point is CoursePoint {
                    message = Utils.localizedString(forKey: "warning_delete_used_waypoint", app: "SARANav")
                } else if point is Mark {
                    message = Utils.localizedString(forKey: "warning_delete_used_mark", app: "SARANav")
                }
                let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: message, preferredStyle: .alert)
                let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { _ in
                    self.viewModel?.currentPointIndex = NavigationManager.shared.currentPointIndex.value
                    if self.viewModel?.currentPointIndex != nil && (self.viewModel?.currentPointIndex)! > 0 {
                        self.viewModel?.currentPointIndex! -= 1
                    }
                    self.viewModel?.removeFromRoute(at: indexPath)
                    tableView.deleteRows(at: [indexPath], with: .automatic)
                }
                let noAction = UIAlertAction(title: NSLocalizedString("cancel", bundle: bundle, comment: ""), style: .cancel, handler: nil)
                alertController.addAction(noAction)
                alertController.addAction(yesAction)
                present(alertController, animated: true, completion: nil)
            } else {
                self.viewModel?.removeFromRoute(at: indexPath)
                tableView.deleteRows(at: [indexPath], with: .automatic)
                self.uploadButton.isEnabled = User.shared.email.value != nil && ((viewModel?.numberOfPoints() ?? 0) >= 1) && self.nameTxtField.text?.isNotEmpty ?? false
            }
        }
    }

    public func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    public func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {

        //Retrieve point to move
        guard let pointToMove = self.viewModel?.getPointAtIndexPath(sourceIndexPath) else {
            return
        }

        let previousIndexPath = destinationIndexPath.row < sourceIndexPath.row ? IndexPath(row: destinationIndexPath.row - 1, section: destinationIndexPath.section) : destinationIndexPath

        let nextIndexPath = destinationIndexPath.row < sourceIndexPath.row ? destinationIndexPath : IndexPath(row: destinationIndexPath.row + 1, section: destinationIndexPath.section)

        if previousIndexPath.row > 0 {
            if self.viewModel?.getPointAtIndexPath(previousIndexPath)?.id == pointToMove.id {
                let alert = Utils.showAlert(with: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("warning_same_point_consecutively", bundle: bundle, comment: ""))

                self.present(alert, animated: true, completion: nil)
                stopEditing()
                tableView.reloadData()
                return
            }
        }

        if nextIndexPath.row < self.viewModel?.numberOfPoints() ?? 0 {

            if self.viewModel?.getPointAtIndexPath(nextIndexPath)?.id == pointToMove.id {
                let alert = Utils.showAlert(with: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("warning_same_point_consecutively", bundle: bundle, comment: ""))

                self.present(alert, animated: true, completion: nil)
                stopEditing()
                tableView.reloadData()
                return
            }
        }
        //Remove it from old position and insert it to new position
        self.viewModel?.removeFromRoute(at: sourceIndexPath)
        self.viewModel?.addToRoute(pointToMove, at: destinationIndexPath)
    }
    
   //MARK: - SelectButtonCellDelegate
    public func didSelectCell(id: String) {
        
    }
    
    // MARK: - AccessibleSwitch
    func switchAction() {
        self.viewModel?.reversedRoute = !(self.viewModel?.reversedRoute)!
        self.viewModel?.reversePoints()
        self.tableView.reloadData()
    }

}
