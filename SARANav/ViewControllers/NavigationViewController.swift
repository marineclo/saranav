//
//  ViewController.swift
//  SARA Croisiere
//
//  Created by Rose on 24/07/2017.
//  Copyright © 2017 Rose. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AVFoundation
import SARAFramework

class NavigationViewController: UIViewController, SelectionPointDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet private weak var nextButton: UIButton!
    @IBOutlet private weak var previousButton: UIButton!
    @IBOutlet private weak var chooseCourseButton: UIButton!
    
    @IBOutlet private weak var pagingControl: AccessiblePageControl!
    @IBOutlet private weak var choosePointButton: UIButton!
    
    @IBOutlet private weak var courseQuickInfo: ImageLabel!
    @IBOutlet private weak var speedQuickInfo: ImageLabel!
    @IBOutlet weak var compasQuickInfo: ImageLabel!
    
    @IBOutlet private weak var muteButton: UIButton!
    @IBOutlet weak var tabsMenuContainerView: UIView!
    
    // MARK: - Variables
    let bundle = Utils.bundle(anyClass: NavigationViewController.classForCoder())
    
    private var navigationPagingController: NavigationPagingViewController? {
        didSet {
            navigationPagingController?.tabsMenuViewController = tabsMenuController
            tabsMenuController?.delegate = navigationPagingController
        }
    }
    private var tabsMenuController: TabsMenuViewController? {
        didSet {
            navigationPagingController?.tabsMenuViewController = tabsMenuController
            tabsMenuController?.delegate = navigationPagingController
        }
    }
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarItem.selectedImage = UIImage(named: "sailboatTabBar", in: bundle, compatibleWith: nil)
        
    
        // Setup UI
        self.view.backgroundColor = Utils.primaryColor
        chooseCourseButton.customizeReverse()
        chooseCourseButton.setImage(UIImage(named: "route", in: self.bundle, compatibleWith: nil), for: .normal)
        chooseCourseButton.imageView?.tintColor = Utils.secondaryColor
        
        previousButton.setTitleColor(Utils.disabledColor, for: .disabled)
        previousButton.accessibilityLabel = NSLocalizedString("activate_previous_point", bundle: bundle, comment: "")
        previousButton.setImage(UIImage(named: "left-chevron", in: self.bundle, compatibleWith: nil), for: .normal)
        previousButton.tintColor = Utils.secondaryColor
        nextButton.setTitleColor(Utils.disabledColor, for: .disabled)
        nextButton.accessibilityLabel = NSLocalizedString("activate_next_point", bundle: bundle, comment: "")
        nextButton.setImage(UIImage(named: "right-chevron", in: self.bundle, compatibleWith: nil), for: .normal)
        nextButton.tintColor = Utils.secondaryColor
        
        choosePointButton.customizeReverse()
        choosePointButton.setImage(UIImage(named: "waypoint", in: self.bundle, compatibleWith: nil), for: .normal)
        choosePointButton.imageView?.tintColor = Utils.secondaryColor
        
        muteButton.setImage(UIImage(named: "volume-up", in: self.bundle, compatibleWith: nil), for: .normal)
        muteButton.accessibilityLabel = NSLocalizedString("deactivate_announces", bundle: bundle, comment: "")
        
        NavigationManager.shared.shouldReloadUnits
        .asObservable()
        .filter({ $0 == true })
        .subscribe(onNext: { [weak self] _ in
            self?.speedQuickInfo.quickInfoUnit = Settings.shared.speedUnit?.unit
        }).disposed(by: disposeBag)
        
        //Bind variables
        // Set types of the quick info top bar
        courseQuickInfo.quickInfoType = .course
        speedQuickInfo.quickInfoType = .speed
        compasQuickInfo.quickInfoType = .compass
        
        LocationManager.shared.compassVariable
            .asObservable()
            .map({
                if $0 != nil {
                    return String(format: "%i", Int($0!.trueHeading))
                } else {
                    return NSLocalizedString("unavailable_nav_symbol", bundle: self.bundle, comment: "")
                }
            })
            .bind(to: compasQuickInfo.value)
            .disposed(by: disposeBag)
        
        NavigationManager.shared.speedOverGround
            .asObservable()
            .map({
                if $0 != nil {
                    return String($0!)
                } else {
                    return NSLocalizedString("unavailable_nav_symbol", bundle: self.bundle, comment: "")
                }
            })
            .bind(to: speedQuickInfo.value)
            .disposed(by: disposeBag)
        
        NavigationManager.shared.courseOverGround
            .asObservable()
            .map({
                if $0 != nil {
                    return String(format: "%i", $0!)
                } else {
                    return NSLocalizedString("unavailable_nav_symbol", bundle: self.bundle, comment: "")
                }
            })
            .bind(to: courseQuickInfo.value)
            .disposed(by: disposeBag)
        
        NavigationManager.shared.isNotFirstPoint
            .asObservable()
            .bind(to: previousButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        NavigationManager.shared.isNotLastPoint
            .asObservable()
            .bind(to: nextButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        NavigationManager.shared.route
            .asObservable()
            .subscribe({ (_) in
                let course = NavigationManager.shared.route.value
                let name = NavigationManager.shared.route.value?.name ?? Utils.localizedString(forKey: "choose_route", app: "SARANav")
                self.chooseCourseButton.setTitle(name, for: .normal)
                
                if course == nil {
                    self.chooseCourseButton.accessibilityLabel = "\(Utils.localizedString(forKey: "choose_routes_list", app: "SARANav")) \(Utils.localizedString(forKey: "no_activated_route", app: "SARANav"))"
                    if let viewRightImage = self.chooseCourseButton.viewWithTag(100) {
                        viewRightImage.removeFromSuperview()
                    }
                } else {
                    if let cruiseCourse = course as? CruiseCourse, cruiseCourse.reversed {
                        self.chooseCourseButton.addRightImage(imagePadding: 20, image: UIImage(named: "undo",in: self.bundle, compatibleWith: nil))
                        self.chooseCourseButton.accessibilityLabel = "\(Utils.localizedString(forKey: "choose_routes_list", app: "SARANav"))  \(Utils.localizedString(forKey: "route_title", app: "SARANav")) \(cruiseCourse.name) \(Utils.localizedString(forKey: "reversed_route", app: "SARANav")) \(NSLocalizedString("activated", bundle: self.bundle, comment: ""))"
                    } else {
                        if let viewRightImage = self.chooseCourseButton.viewWithTag(100) {
                            viewRightImage.removeFromSuperview()
                        }
                        self.chooseCourseButton.accessibilityLabel = "\(Utils.localizedString(forKey: "choose_routes_list", app: "SARANav")) \(Utils.localizedString(forKey: "route_title", app: "SARANav")) \(course!.name) \(NSLocalizedString("activated", bundle: self.bundle, comment: ""))"
                    }
                }
                self.chooseCourseButton.imageView?.contentMode = .scaleAspectFit
                self.chooseCourseButton.moveImageLeftTextCenter()
            }).disposed(by: disposeBag)
                
        NavigationManager.shared.point
            .asObservable()
            .subscribe(onNext: { (anyPoint) in
                let name = anyPoint?.point.name ?? NSLocalizedString("choose_point", bundle: self.bundle, comment: "")
                self.choosePointButton.setTitle(name, for: .normal)
                
                if anyPoint == nil {
                    self.choosePointButton.accessibilityLabel = "\(NSLocalizedString("choose_points_list", bundle: self.bundle, comment: "")) \(NSLocalizedString("no_activated_point", bundle: self.bundle, comment: ""))"
                } else {
                    self.choosePointButton.accessibilityLabel = "\(NSLocalizedString("choose_points_list", bundle: self.bundle, comment: "")) \(NSLocalizedString("point_title", bundle: self.bundle, comment: "")) \(name) \(NSLocalizedString("activated", bundle: self.bundle, comment: ""))"
                }
                self.choosePointButton.alignTextUnderImage()
            }).disposed(by: disposeBag)
        
        // Tabs menu
        self.tabsMenuController = TabsMenuViewController()
        self.addChild(self.tabsMenuController!)
        self.tabsMenuContainerView.addSubview(tabsMenuController!.view)
        self.tabsMenuController!.view.frame = CGRect(x: 0, y: 0, width: self.tabsMenuContainerView.frame.size.width, height: self.tabsMenuContainerView.frame.size.height)

        self.tabsMenuController!.menuBackgroundColor = Utils.primaryColor
        self.tabsMenuController!.menuItemsColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if NavigationManager.shared.route.value != nil {
            chooseCourseButton.isEnabled = true
            chooseCourseButton.moveImageLeftTextCenter()
            choosePointButton.alignTextUnderImage()
            muteButton.isEnabled = true
        } else {
            chooseCourseButton.isEnabled = Course.all().count != 0
//            muteButton.isEnabled = CoursePoint.all().count != 0
        }
    }
    
    override func viewDidLayoutSubviews() {
        muteButton.layer.cornerRadius = muteButton.frame.size.width / 2
        muteButton.layer.borderWidth = 1
        muteButton.imageView?.contentMode = .scaleAspectFit
        if AnnouncementQueueManager.shared.muted {
            muteButton.tintColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            muteButton.layer.borderColor = Utils.secondaryColor.cgColor
        } else {
            muteButton.tintColor = Utils.secondaryColor
            muteButton.layer.borderColor = Utils.secondaryColor.cgColor
        }
    }
    
    // MARK: - IBAction
    /// Go to next point
    ///
    /// - Parameter sender: Button
    @IBAction func pressNext(_ sender: Any) {
        NavigationManager.shared.setNextPoint()
    }
    
    /// Go to previous point
    ///
    /// - Parameter sender: Button
    @IBAction func pressPrevious(_ sender: Any) {
        NavigationManager.shared.setPreviousPoint()
    }
 
    /// Open actionsheet to choose the route
    ///
    /// - Parameter sender: Choose button
    @IBAction func chooseCourse(_ sender: Any) {
        let coursesMenu = UIAlertController(title: nil, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        
        // Adding deselect current route action if there is a route selected
        if NavigationManager.shared.route.value != nil {
            let unselectAction = UIAlertAction(title: Utils.localizedString(forKey: "deactivate_route", app: "SARANav"), style: .destructive, handler: { _ in
                    NavigationManager.shared.setCurrentRoute(route: nil)
                    NavigationManager.shared.isNotFirstPoint.accept(false)
                    NavigationManager.shared.isNotLastPoint.accept(false)
                    if let viewRightImage = self.chooseCourseButton.viewWithTag(100) {
                        viewRightImage.removeFromSuperview()
                    }
                    UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self.chooseCourseButton)
            })
            coursesMenu.addAction(unselectAction)
        }
        
        for course in Course.allSortedByProximityFirstPoint() {
            // Create action for each of the route in the app
            let courseAction = UIAlertAction(title: course.name, style: .default, handler: { [weak self] _ in
                guard let weakSelf = self else {
                    return
                }
                if course.points.isEmpty {
                    let alert = UIAlertController(title: NSLocalizedString("warning", bundle: weakSelf.bundle, comment: ""), message: NSLocalizedString("warning_no_point_route", bundle: weakSelf.bundle, comment: ""), preferredStyle: .alert)
                    let okAction = UIAlertAction(title: NSLocalizedString("ok", bundle: weakSelf.bundle, comment: ""), style: .default, handler: nil)
                    alert.addAction(okAction)
                    self?.present(alert, animated: true, completion: nil)
                    
                } else if !course.allPointsHasCoordinates() {
                    let alert = UIAlertController(title: NSLocalizedString("warning", bundle: weakSelf.bundle, comment: ""), message: Utils.localizedString(forKey: "warning_route_point_no_coordinates", app: "SARANav"), preferredStyle: .alert)
                    let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: weakSelf.bundle, comment: ""), style: .default) { _ in
                        NavigationManager.shared.setCurrentRoute(route: course)
                        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self?.chooseCourseButton)
                    }
                    let noAction = UIAlertAction(title: NSLocalizedString("no", bundle: weakSelf.bundle, comment: ""), style: .cancel, handler: {_ in
                        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self?.chooseCourseButton)
                    })
                    alert.addAction(noAction)
                    alert.addAction(yesAction)
                    self?.present(alert, animated: true, completion: nil)
                } else {
                    NavigationManager.shared.setCurrentRoute(route: course)
                    UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self?.chooseCourseButton)
                }
                
            })
            coursesMenu.addAction(courseAction)
        }
        
        // Adding cancel action to go back
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", bundle: self.bundle, comment: ""), style: .cancel, handler: nil)
        cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
        coursesMenu.addAction(cancelAction)
        
        // Present action sheet
        present(coursesMenu, animated: true, completion: nil)
    }
    
    @IBAction func pressMute(_ sender: Any) {
        if AnnouncementQueueManager.shared.muted {
            muteButton.setImage(UIImage(named: "volume-up", in: bundle, compatibleWith: nil), for: .normal)
            AnnouncementQueueManager.shared.muted = false
            self.muteButton.accessibilityLabel = NSLocalizedString("deactivate_announces", bundle: bundle, comment: "")
            if !UIAccessibility.isVoiceOverRunning {
                AnnouncementQueueManager.shared.addAnnounce(Announce(text: NSLocalizedString("activate_announces_message", bundle: bundle, comment: ""), priority: 0, type: .mute, annonceState: nil))
            } else {
                let when = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: when) {
                    UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: NSLocalizedString("activate_announces_message", bundle: self.bundle, comment: ""))
                }
            }
        } else {
            muteButton.setImage(UIImage(named: "volume-down", in: bundle, compatibleWith: nil), for: .normal)
            AnnouncementQueueManager.shared.muted = true
            self.muteButton.accessibilityLabel = NSLocalizedString("activate_announces", bundle: bundle, comment: "")
            if !UIAccessibility.isVoiceOverRunning {
                AnnouncementQueueManager.shared.addAnnounce(Announce(text: NSLocalizedString("deactivate_announces_message", bundle: bundle, comment: ""), priority: 0, type: .mute, annonceState: nil))
            } else {
                let when = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: when) {
                    UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: NSLocalizedString("deactivate_announces_message", bundle: self.bundle, comment: ""))
                }
            }
        }
    }
    
    @IBAction func choosePoint(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Route", bundle: bundle)
        let ptvc = storyBoard.instantiateViewController(withIdentifier: "pointsNavigation") as! UINavigationController
        (ptvc.viewControllers[0] as! PointsViewController).selectionMode = true
        (ptvc.viewControllers[0] as! PointsViewController).delegate = self
        self.present(ptvc, animated: true, completion: nil)
    }
    
    //MARK: - SelectionPointDelegate implementation
    func selectionPoint(point: PointRealm) {
        //Create new route with selected point
        let route = Course(id: "", name: "\(NSLocalizedString("to_a_waypoint", bundle: bundle, comment: "")): \(point.name)", points: [point.anyPoint()!])
        NavigationManager.shared.setCurrentRoute(route: route, isSaved: false)
    }
    
    // MARK: - Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "navigationPage" {
            guard let controller = segue.destination as? NavigationPagingViewController else {
                return
            }
            // Set items we need
            controller.infoTypes = [.gps, .route, .navigationCenter, .telltales, .seascape] 
            self.navigationPagingController = controller
        }
    }
}
