//
//  RoutesViewController.swift
//  SARANav
//
//  Created by Marine on 06.10.23.
//  Copyright © 2023 GraniteApps. All rights reserved.
//

import UIKit
import SARAFramework

class RoutesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SelectButtonCellDelegate {
    
    // MARK: - Outlets
    @IBOutlet private var tableView: UITableView?
    @IBOutlet private weak var createRouteButton: UIButton?

    // MARK: - Variables
    var routes: [Course]?
    let bundle = Utils.bundle(anyClass: RoutesViewController.classForCoder())
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = Utils.localizedString(forKey: "menu_routes", app: "SARANav")
        
        self.createRouteButton?.customizePrimary()
        self.createRouteButton?.setTitle(Utils.localizedString(forKey: "create_route", app: "SARANav"), for: .normal)
        self.createRouteButton?.accessibilityLabel = Utils.localizedString(forKey: "create_route", app: "SARANav")

        self.tableView?.customize()
        self.tableView?.tableFooterView = UIView(frame: .zero)
        let bundleView = Bundle(for: RoutesViewController.self)
        self.tableView?.register(UINib(nibName: "SelectButtonCell", bundle: bundleView), forCellReuseIdentifier: "routeCellIdentifier")
        self.tableView?.register(UINib(nibName: "HeaderTableView", bundle: bundleView), forHeaderFooterViewReuseIdentifier: "HeaderTableView")
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(handleModalDismissed), name: NSNotification.Name(rawValue:"modalCreateCruiseIsDimissed"), object: nil)
        self.routes = Course.allSortedByProximityFirstPoint()
        self.tableView?.reloadData()
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func handleModalDismissed() {
      self.routes = Course.allSortedByProximityFirstPoint()
      self.tableView?.reloadData()
    }
    
    // MARK: - Table View
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.routes?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "routeCellIdentifier") as? SelectButtonCell else {
            return UITableViewCell()
        }
        
        cell.delegate = self
        cell.titleLabel?.font = UIFont.preferredFont(forTextStyle: .body)
        cell.titleLabel?.text = self.routes?[indexPath.row].name
        cell.idElementSelected = self.routes?[indexPath.row].id
        if let course = self.routes?[indexPath.row] as? CruiseCourse, course.reversed {
            cell.inverseImageView.isHidden = false
            cell.inverseImageView.image = UIImage(named: "undo", in: bundle, compatibleWith: nil)
            cell.inverseImageView.tintColor = Utils.primaryColor
            cell.titleLabel?.accessibilityLabel = "\(self.routes?[indexPath.row].name ?? ""), \(Utils.localizedString(forKey: "reversed_route", app: "SARANav"))"
            cell.inversedImageConstraint?.isActive = false
        }  else {
            cell.titleLabel?.accessibilityLabel = "\(self.routes?[indexPath.row].name ?? ""))"
            cell.inverseImageView.isHidden = true
            cell.inversedImageConstraint?.isActive = true
        }
                
        if self.routes?[indexPath.row] == NavigationManager.shared.route.value {
            cell.selectButton?.accessibilityLabel = "\(Utils.localizedString(forKey: "activated_route", app: "SARANav")) \(NavigationManager.shared.route.value!.name)"
            cell.selectButton?.setImage(UIImage(named: "globe", in: bundle, compatibleWith: nil), for: .disabled)
            cell.selectButton?.isEnabled = false
            cell.selectButton?.tintColor = Utils.tintIcon
        } else {
            let course = self.routes?[indexPath.row]
            cell.selectButton?.setImage(UIImage(named: "circle", in: bundle, compatibleWith: nil), for: .normal)
            if course?.points.count == 0 { // The course has no points
                cell.selectButton?.tintColor = UIColor.lightGray
                cell.selectButton?.isEnabled = false
                cell.selectButton?.accessibilityLabel = "\(Utils.localizedString(forKey: "route_no_points_not_activable", app: "SARANav")))"
            } else {
                cell.selectButton?.isEnabled = true
                cell.selectButton?.accessibilityLabel = "\(Utils.localizedString(forKey: "activate_route", app: "SARANav")) \(self.routes?[indexPath.row].name ?? "")"
                cell.selectButton?.tintColor = Utils.tintIcon
            }
            
            cell.accessoryType = .none
        }
        cell.accessibilityTraits = .button
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: RoutesViewController.classForCoder()))
        guard let controller = storyboard.instantiateViewController(withIdentifier: "CreateRouteViewController") as? CreateRouteViewController,
            let route = self.routes?[indexPath.row] else {
            return
        }
        
        controller.setCourse(route)
        let backItem = UIBarButtonItem()
        backItem.title = NSLocalizedString("cancel", bundle: bundle, comment: "")
        navigationItem.backBarButtonItem = backItem
        self.present(controller, animated: true, completion: nil)
    }
       
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = self.tableView?.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableView" ) as! HeaderTableView
        returnedView.customizeHeader(titleName: Utils.localizedString(forKey: "routes_list", app: "SARANav"))
        return returnedView
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    //MARK: - SelectButtonCellDelegate
    public func didSelectCell(id: String) {
        guard let route = self.routes?.first(where: {$0.id == id}) else {
            return
        }
        if route.points.isEmpty {
            let alert = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: Utils.localizedString(forKey: "warning_no_point_route", app: "SARANav"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("ok", bundle: bundle, comment: ""), style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            
        } else if !route.allPointsHasCoordinates() {
            let alert = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: Utils.localizedString(forKey: "warning_route_point_no_coordinates", app: "SARANav"), preferredStyle: .alert)
            let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { _ in
                NavigationManager.shared.updateNavView = true
                NavigationManager.shared.setCurrentRoute(route: route)
            }
            let noAction = UIAlertAction(title: NSLocalizedString("no", bundle: bundle, comment: ""), style: .cancel, handler: nil)
            alert.addAction(noAction)
            alert.addAction(yesAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            NavigationManager.shared.updateNavView = true
            NavigationManager.shared.setCurrentRoute(route: route)
        }
    }
}
