//
//  TabBarViewController.swift
//  SARANav
//
//  Created by Marine on 12.05.20.
//  Copyright © 2020 GraniteApps. All rights reserved.
//

import UIKit
import SARAFramework
import RxSwift
import FirebaseAuth

class TabBarViewController: UITabBarController, UITabBarControllerDelegate, AnnouncementDelegate, NmeaManagerDelegate, LocalizableString, FirebaseManagerDelegate {

    // MARK: Variables
    let bundle = Utils.bundle(anyClass: TabBarViewController.classForCoder())
    let disposeBag = DisposeBag()
    
    // MARK: - 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.delegate = self // UITabBarControllerDelegate
        NmeaManager.default.nmeaDelegate = self
        NavigationManager.shared.announcementDelegate = self
        FirebaseManager.shared.delegate = self
        AnnouncementManager.shared.announcementDelegate = self
        Utils.localizableProtocol = self
        FirebaseManager.shared.retrieveAllCourses()
        
        NavigationManager.shared.route
            .asObservable()
            .subscribe(onNext: { [weak self] (_) in
                if NavigationManager.shared.updateNavView { // When a route is activated
                    self?.selectedIndex = 0 // Go to the navigation tabs
                }
            }).disposed(by: disposeBag)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        // Set the tab bar title at runtime
        guard let items = self.tabBar.items else {
            return
        }
        
        items[0].image = UIImage(named: "sailboatTabBar", in: bundle, compatibleWith: nil)
        items[1].image = UIImage(named: "itineraryTabBar", in: bundle, compatibleWith: nil)
        items[2].image = UIImage(named: "bell", in: bundle, compatibleWith: nil)
        items[3].image = UIImage(named: "mapTabBar", in: bundle, compatibleWith: nil)
        items[4].image = UIImage(named: "settingsTabBar", in: bundle, compatibleWith: nil)

        items[0].title = NSLocalizedString("navigation", bundle: bundle, comment: "")
        items[1].title = NSLocalizedString("menu_itinerary", bundle: bundle, comment: "")
        items[2].title = NSLocalizedString("menu_announces", bundle: bundle, comment: "")
        items[3].title = NSLocalizedString("menu_carto", bundle: bundle, comment: "")
        items[4].title = NSLocalizedString("menu_settings", bundle: bundle, comment: "")
        
        if let vc = viewControllers?[1] as? CoursesMenuViewController {
            vc.setNavigationTitle(title: NSLocalizedString("menu_itinerary", bundle: bundle, comment: ""))
            let tabsMenuItems = [
                Utils.localizedString(forKey: "menu_routes", app: "SARANav"),
                NSLocalizedString("menu_points", bundle: bundle, comment: ""),
                NSLocalizedString("menu_download", bundle: bundle, comment: "")
            ]
            vc.setTabMenuItems(items: tabsMenuItems)
            // Embedded view controllers
            var pagesViewControllers: [UIViewController] = []
            // Routes
            if let routesViewController = UIStoryboard(name: "Main", bundle: Bundle(for: TabBarViewController.classForCoder())).instantiateViewController(withIdentifier: "RoutesViewController") as? RoutesViewController {
                pagesViewControllers.append(routesViewController)
            }
            let storyboard = UIStoryboard(name: "Route", bundle: Bundle(for: CoursesMenuViewController.classForCoder()))
            // Points
            if let coursePointsViewController = storyboard.instantiateViewController(withIdentifier: "PointsViewController") as? PointsViewController {
                pagesViewControllers.append(coursePointsViewController)
            }
            // Download
            if let downloadRouteViewController = storyboard.instantiateViewController(withIdentifier: "DownloadRouteViewController") as? DownloadRouteViewController {
                pagesViewControllers.append(downloadRouteViewController)
            }
            vc.setPagesViewController(viewControllers: pagesViewControllers)
        }
    }
    
    // MARK: - AnnouncementDelegate
    // Announcement for crossing point that is not the last one of the course
    func announcementPointValidation(nextPoint: AnyPoint?, noCoordMessage: String?) -> String {
        var nextPointAnnounce = ""
        if let p = nextPoint?.point as? CoursePoint {
            nextPointAnnounce = p.name + " \(NSLocalizedString("waypoint", bundle: bundle, comment: ""))"
        } else if let m = nextPoint?.point as? Mark {
            if m.getMarkType() == Mark.MarkType.simple.rawValue {
                if m.buoys.first!.toLetAt(inversed: (NavigationManager.shared.route.value as? CruiseCourse)?.reversed) == Buoy.ToLetAt.port.rawValue {
                    nextPointAnnounce = m.name + " \(NSLocalizedString("mark_port", bundle: bundle, comment: ""))"
                } else {
                    nextPointAnnounce = m.name + " \(NSLocalizedString("mark_starboard", bundle: bundle, comment: ""))"
                }
            } else {
                nextPointAnnounce = m.name + " \(NSLocalizedString("mark_double", bundle: bundle, comment: ""))"
            }
        }
        guard let currentPoint = NavigationManager.shared.point.value else { return "" }
        return String(format: NSLocalizedString("announce_next_point", bundle: bundle, comment: ""), currentPoint.point.name, noCoordMessage ?? "", nextPointAnnounce)
    }
    
    // Announcement for last point in the course
    func announcementEndRoute(noCoordMessage: String?, noCoord: Bool) -> String {
        guard let currentPoint = NavigationManager.shared.point.value, let currentRoute = NavigationManager.shared.route.value else {
            return ""
        }
        return noCoord ? String(format: Utils.localizedString(forKey: "announce_end_route", app: "SARANav"), noCoordMessage ?? "", currentRoute.name) : String(format: Utils.localizedString(forKey: "announce_end_route_point", app: "SARANav"), currentPoint.point.name, noCoordMessage ?? "", currentRoute.name)
    }
    
    func profileRouteTitle() -> String {
        return Utils.localizedString(forKey: "route_title", app: "SARANav")
    }
    
    func deleteMarkUsedRouteMessage() -> String {
        return Utils.localizedString(forKey: "warning_delete_mark_route", app: "SARANav")
    }
    
    // MARK: - NmeaManagerDelegate
    func displayWarningGPS() {
        let alert = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("warning_central_gps", bundle: bundle, comment: ""), preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default, handler: {_ in
            Settings.shared.setUsePhoneGPS(value: true)
            if let vc = (self.selectedViewController as? UINavigationController)?.visibleViewController as? NMEASettingsViewController{
                vc.gpsSourceSegmentedControl.selectedSegmentIndex = 0
            }
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("no", bundle: bundle, comment: ""), style: .cancel, handler: nil))

        self.present(alert, animated: true)
    }
    
    // MARK: - LocalizableString
    func aboutText() -> String {
        return Utils.localizedString(forKey: "about", app: "SARANav")
    }
    
    // MARK: - FirebaseManagerDelegate
    func courseChanged(course: SARAFramework.Course?) {}
    
    func courseRemoved(courseId: String) {}
    
    func courseAdded(course: SARAFramework.Course?) {}
}
