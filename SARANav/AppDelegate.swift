//
//  AppDelegate.swift
//  SARANav
//
//  Created by Marine on 26.05.21.
//  Copyright © 2021 GraniteApps. All rights reserved.

import UIKit
import Firebase
import RealmSwift
import AVFoundation
import SARAFramework

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var tabBarController: TabBarViewController?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Firebase
        var directory: String
        var nameDatabase: String
        if BuildConfiguration.shared.environment == .debugDevelopment {
            print("DEBUG")
            directory = "Debug"
            nameDatabase = "debug"
        } else if BuildConfiguration.shared.environment == .releaseProd {
            print("RELEASE")
            directory = "Prod"
            nameDatabase = "prod"
        } else {
            print("TESTFLIGHT")
            directory = "TestFlight"
            nameDatabase = "testFlight"
        }
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let docURL = URL(string: documentsDirectory)!
        let dataPath = docURL.appendingPathComponent(directory)
        if !FileManager.default.fileExists(atPath: dataPath.absoluteString) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath.absoluteString, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
            }
        }
        
        // To move the default database to production in case of the user uses the app before the 3 databases: debug, prod, testFlight
        moveItem(docURL: docURL, pathExtension: "realm")
        moveItem(docURL: docURL, pathExtension: "realm.backup-log")
        moveItem(docURL: docURL, pathExtension: "realm.lock")
        moveItem(docURL: docURL, pathExtension: "realm.management")
        
        let config = Realm.Configuration(
            fileURL: dataPath.appendingPathComponent("\(nameDatabase).realm"),
            schemaVersion: 9,//32,  //Increment this each time your schema changes: put 5
            migrationBlock: { migration, oldSchemaVersion in
                if oldSchemaVersion < 2 {
                    // Delete old MOB
                    var gpsPsIdToDelete: [String] = []
                    migration.enumerateObjects(ofType: CoursePoint.className(), { oldObject, newObject in
                        if let distance = oldObject?["distanceThreshold"] as? Float {
                            if distance == Float.greatestFiniteMagnitude {
                                if let gpsPosId = (oldObject?["gpsPosition"] as! MigrationObject)["id"] as? String {
                                    gpsPsIdToDelete.append(gpsPosId)
                                }
                                migration.delete(newObject!)
                            }
                        }
                    })
                    migration.enumerateObjects(ofType: GPSPosition.className(), { oldObject, newObject in
                        if let id = oldObject?["id"] as? String, gpsPsIdToDelete.contains(id) {
                            migration.delete(newObject!)
                        }
                    })
                }
                if oldSchemaVersion < 3 {
                    migration.enumerateObjects(ofType: AutoCreationParams.className(), { _, newObject in
                        newObject!["id"] = ""
                        newObject!["startLength"] = 200
                        newObject!["startWindwardLength"] = 700
                        newObject!["finishLength"] = 200
                        newObject!["leewardFinishLength"] = 400
                        newObject!["offsetMarkLength"] = 100
                        newObject!["refBuoy"] = nil
                    })
                    migration.enumerateObjects(ofType: Buoy.className(), { _, newObject in
                        newObject!["type"] = nil
                    })
                    migration.enumerateObjects(ofType: ProfileRouteRealm.className(), { _, newObject in
                        newObject!["entry3Lengths"] = nil
                    })
                    migration.enumerateObjects(ofType: RaceCourse.className(), { oldObject, newObject in
                        newObject!["type"] = Int(oldObject!["type"] as! String)
                    })
                }
                if oldSchemaVersion < 4 {
                    migration.enumerateObjects(ofType: Settings.className(), { _, newObject in
                        newObject!["useCompass"] = false
                    })
                }
                if oldSchemaVersion < 5 {
                    migration.enumerateObjects(ofType: ProfileRealm.className(), { oldObject, newObject in
                        if let profiles = oldObject!["profilesTypeRealm"] as? List<MigrationObject>, profiles.count < 6 { // No carto profile: need to add one
                            let detectionDistanceProfile = migration.create(ProfileSetting.className(), value: ["id" : UUID().uuidString, "isActivated": false, "threshold": 500, "timeThreshold": 0])
                            let detectionNumberProfile = migration.create(ProfileSetting.className(), value: ["id" : UUID().uuidString, "isActivated": false, "threshold": 3, "timeThreshold": 0])
                            let distance = migration.create(ProfileSetting.className(), value: ["id" : UUID().uuidString, "isActivated": false, "threshold": 10, "timeThreshold": 10])
                            let gisement = migration.create(ProfileSetting.className(), value: ["id" : UUID().uuidString, "isActivated": false, "threshold": 10, "timeThreshold": 10])
                            let gisementTime = migration.create(ProfileSetting.className(), value: ["id" : UUID().uuidString, "isActivated": false, "threshold": 1, "timeThreshold": 10])
                            let azimut = migration.create(ProfileSetting.className(), value: ["id" : UUID().uuidString, "isActivated": false, "threshold": 10, "timeThreshold": 10])
                            let onlyFront = migration.create(ProfileSetting.className(), value: ["id" : UUID().uuidString, "isActivated": true, "threshold": 0, "timeThreshold": 0])
                            let uuidProfileCartoRealm = UUID().uuidString
                            migration.create(ProfileCartoRealm.className(), value: ["id" : uuidProfileCartoRealm, "detectionDistance" : detectionDistanceProfile, "detectionNumber" : detectionNumberProfile, "distance": distance, "azimut": azimut, "gisement": gisement, "gisementTime": gisementTime, "onlyFront": onlyFront])
                            let anyProfileCarto = migration.create(AnyProfileTypeRealm.className(), value: ["typeName": "ProfileCartoRealm", "primaryKey": uuidProfileCartoRealm])
                            (newObject?["profilesTypeRealm"] as! List<MigrationObject>).append(anyProfileCarto)
                        }
                    })
                    migration.enumerateObjects(ofType: ProfileRouteRealm.className(), { _, newObject in
                        let gisementTime = migration.create(ProfileSetting.className(), value: ["id" : UUID().uuidString, "isActivated": false, "threshold": 1, "timeThreshold": 10])
                        newObject!["gisementTime"] = gisementTime
                    })
                }
                if oldSchemaVersion < 6 {
                    var profileId: String?
                    migration.enumerateObjects(ofType: "ProfileVoiceRealm") { oldObject, newObject in
                        guard let oldObject = oldObject else { return }
                        guard let id = oldObject["id"] as? String else { return }
                        guard let vocalRate = oldObject["vocalRate"] as? Float else { return }
                        guard let voice = oldObject["voice"] as? String else { return }
                        guard let batteryLevel = oldObject["batteryLevel"] as? Object else { return }
                        
                        let rProfileSetting: MigrationObject = migration.create("ProfileSetting")
                        profileId = batteryLevel["id"] as? String
                        rProfileSetting["id"] = batteryLevel["id"]
                        rProfileSetting["isActivated"] = batteryLevel["isActivated"]
                        rProfileSetting["threshold"] = batteryLevel["threshold"]
                        rProfileSetting["timeThreshold"] = batteryLevel["timeThreshold"]
                        
                        let rVarious: MigrationObject = migration.create("ProfileVariousRealm")
                        rVarious["id"] = id
                        rVarious["vocalRate"] = vocalRate * 100
                        rVarious["voice"] = voice
                        rVarious["batteryLevel"] = rProfileSetting
                    }
                    migration.deleteData(forType: "ProfileVoiceRealm")
                    var isDeleted: Bool = false
                    migration.enumerateObjects(ofType: "ProfileSetting", { oldObject, newObject in
                        guard let id = oldObject?["id"] as? String else { return }
                        if profileId != nil && id == profileId! && newObject != nil && !isDeleted {
                            migration.delete(newObject!)
                            isDeleted = true
                        }
                            
                    })
                    migration.enumerateObjects(ofType: "AnyProfileTypeRealm", { oldObject, newObject in
                        guard let oldObject = oldObject else { return }
                        guard let typeName = oldObject["typeName"] as? String else { return }
                        if typeName == "ProfileVoiceRealm" {
                            newObject?["typeName"] = "ProfileVariousRealm"
                        }
                    })
                    migration.enumerateObjects(ofType: "ProfileCartoRealm", { _, newObject in
                        newObject?["enableBeacons"] = false
                        let timeAnnounceProfile = migration.create(ProfileSetting.className(), value: ["id" : UUID().uuidString, "isActivated": true, "threshold": 10, "timeThreshold": 0])
                        newObject?["timeAnnounce"] = timeAnnounceProfile
                    })
                }
                if oldSchemaVersion < 7 {
                    migration.enumerateObjects(ofType: Settings.className(), { _, newObject in
                        newObject!["saveTrace"] = false
                    })
                }
                if oldSchemaVersion < 8 {
                    migration.enumerateObjects(ofType: CruiseCourse.className(), { _, newObject in
                        newObject!["status"] = CourseStatus.none
                    })
                }
                if oldSchemaVersion < 9 {
                    
                }
            }
        )
                
        Realm.Configuration.defaultConfiguration = config
                
        FirebaseApp.configure()
        
        // Set Default profile
        var profileTypes: [ProfileType] = []
        let defaultValues = ProfileSetting.defaultProfileSettingValues
        let defaultValuesActivated = ProfileSetting.defaultProfileActivatedSettingValues
        let defaultSpeedValues = ProfileSetting.defaultSpeedProfileSettingValues
        let defaultSpeedValuesActivated = ProfileSetting.defaultSpeedProfileActivatedSettingValues
        let profileGPS = ProfileGPS(compass: ProfileSetting(defaultValues: defaultValues), courseOverGround: ProfileSetting(defaultValues: defaultValuesActivated), speedOverGround: ProfileSetting(defaultValues: defaultSpeedValuesActivated), maxSpeedOverGround: ProfileSetting(defaultValues: defaultSpeedValuesActivated))
        profileTypes.append(profileGPS)
        let profileRoute = ProfileRoute(azimut: ProfileSetting(defaultValues: defaultValues), gisement: ProfileSetting(defaultValues: defaultValues), gisementTime: ProfileSetting(isActivated: false, threshold: 1, timeThreshold: 10), distance: ProfileSetting(isActivated: true, threshold: 20, timeThreshold: 10), cmg: ProfileSetting(defaultValues: defaultSpeedValues), distanceToSegment: ProfileSetting(defaultValues: defaultValues))
//        let profileRoute = ProfileRoute(azimut: ProfileSetting(defaultValues: defaultValues), gisement: ProfileSetting(defaultValues: defaultValues), gisementTime: ProfileSetting(isActivated: false, threshold: 1, timeThreshold: 10), distance: ProfileSetting(defaultValues: defaultValues), cmg: ProfileSetting(defaultValues: defaultSpeedValues), distanceToSegment: ProfileSetting(defaultValues: defaultValues))
        profileTypes.append(profileRoute)
        let profileCentrale = ProfileCentrale(depth: ProfileSetting(defaultValues: defaultValues), headingMagnetic: ProfileSetting(defaultValues: defaultValues), speedThroughWater: ProfileSetting(defaultValues: defaultSpeedValues), trueWindSpeed: ProfileSetting(defaultValues: defaultSpeedValues), trueWindAngle: ProfileSetting(defaultValues: defaultValues), apparentWindSpeed: ProfileSetting(defaultValues: defaultSpeedValues), apparentWindAngle: ProfileSetting(defaultValues: defaultValues), trueWindDirection: ProfileSetting(defaultValues: defaultValues), waterTemperature: ProfileSetting(defaultValues: defaultSpeedValues), airTemperature: ProfileSetting(defaultValues: defaultSpeedValues))
        profileTypes.append(profileCentrale)
        let profileTelltale = ProfileTelltale(angle: ProfileSetting(defaultValues: defaultSpeedValues), mediane: ProfileSetting(defaultValues: defaultSpeedValues), average: ProfileSetting(defaultValues: defaultSpeedValues), standardDeviation: ProfileSetting(defaultValues: defaultSpeedValues), variationMax: ProfileSetting(defaultValues: defaultSpeedValues), state: ProfileSetting(defaultValues: defaultValues))
        profileTypes.append(profileTelltale)
        let profileVoice = ProfileVarious(voice: "WomanVoice", vocalRate: 50, batteryLevel: ProfileSetting(defaultValues: defaultValuesActivated))
        profileTypes.append(profileVoice)
        let cartoProfile = ProfileCarto(enableBeacons: false, timeAnnounce: ProfileSetting(isActivated: true, threshold: 10, timeThreshold: 0), detectionDistance: ProfileSetting(isActivated: false, threshold: 2000, timeThreshold: 0), detectionNumber: ProfileSetting(isActivated: false, threshold: 1, timeThreshold: 0), distance: ProfileSetting(defaultValues: defaultValues), azimut: ProfileSetting(isActivated: false, threshold: 10, timeThreshold: 0), gisement: ProfileSetting(isActivated: false, threshold: 90, timeThreshold: 0), gisementTime: ProfileSetting(isActivated: false, threshold: 3, timeThreshold: 0), onlyFront: ProfileSetting(isActivated: false, threshold: 0, timeThreshold: 0))
        profileTypes.append(cartoProfile)
        
        Settings.shared.defaultProfileTypes = profileTypes
        
        Settings.shared.load()
        LocationManager.shared.load()
        
        // Change automatic calculation of the diatance for annoncement to a percentage of the distance
        ProfileRouteRealm.updateDistancePercent()
        
        // Set Color
        Utils.secondaryColor = #colorLiteral(red: 0.7294117647, green: 0.937254902, blue: 0.8352941176, alpha: 1)
        
        // White non-transucent navigatio bar, supports dark appearance
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
            let appearanceTabBar = UITabBarAppearance()
            appearanceTabBar.configureWithOpaqueBackground()
            UITabBar.appearance().standardAppearance = appearanceTabBar
            UITabBar.appearance().scrollEdgeAppearance = appearanceTabBar
        }
        UITabBar.appearance().tintColor = Utils.tintIcon
        UIApplication.shared.isIdleTimerDisabled = true
        
        // Audio playback in background
        if #available(iOS 10.0, *) {
            try! AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.spokenAudio, options: [AVAudioSession.CategoryOptions.interruptSpokenAudioAndMixWithOthers])
        }
        else {
            try? AVAudioSession.sharedInstance().setMode(AVAudioSession.Mode.spokenAudio)
        }
        
        // Should we attempt to connect to NMEA?
//        if Settings.shared.nmeaEnabled {
////            print("=== CONNECT APP DELEGATE")
//            NmeaManager.default.connect()
//        } else {
//
//        }
        self.tabBarController = window?.rootViewController as? TabBarViewController
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        print("== applicationWillResignActive")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        print("== applicationDidEnterBackground")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        print("== applicationWillEnterForeground")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        print("== applicationDidBecomeActive")
    }

    func applicationWillTerminate(_ application: UIApplication) {
        NmeaManager.default.disconnect()
        print("== applicationWillTerminate")
    }
    
    func moveItem(docURL: URL, pathExtension: String) {
        let fileName = "default.\(pathExtension)"
        if FileManager.default.fileExists(atPath: docURL.appendingPathComponent(fileName).absoluteString) {
            if !FileManager.default.fileExists(atPath: docURL.appendingPathComponent("Prod").absoluteString) {
                do {
                    try FileManager.default.createDirectory(atPath: docURL.appendingPathComponent("Prod").absoluteString, withIntermediateDirectories: true, attributes: nil)
                } catch {
                    print(error.localizedDescription)
                }
            }
            do{
                try FileManager.default.moveItem(atPath: docURL.appendingPathComponent(fileName).absoluteString, toPath: docURL.appendingPathComponent("Prod").appendingPathComponent("prod.\(pathExtension)").absoluteString)
            }
            catch{
                print("Error info: \(error)")
            }
        }
    }
}

